package cz.muni.fi.pb162;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicLong;

public class Clerk extends Person {

    public static final AtomicLong counter = new AtomicLong(0);

    private final long id;

    public Clerk(String name, LocalDate dateOfBirth) {
        super(name, dateOfBirth);
        id = counter.getAndIncrement();
    }

    public long getId() {
        return id;
    }

}
