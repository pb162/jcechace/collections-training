package cz.muni.fi.pb162;

import java.time.LocalDate;

public class Client extends Person {

    private final LocalDate registrationDate;

    public Client(String name, LocalDate dateOfBirth, LocalDate registrationDate) {
        super(name, dateOfBirth);
        this.registrationDate = registrationDate;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }
}
