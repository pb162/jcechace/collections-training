package cz.muni.fi.pb162;

import java.time.LocalDate;
import java.util.List;

public class Demo {

    public static void main(String[] args) {
        Office office = new Office();
        var clerks = List.of(
                new Clerk("Pepa Papir", LocalDate.of(2000,11, 2)),
                new Clerk("Marka Sesivacka", LocalDate.of(1974, 2, 2)),
                new Clerk("Tonda Tuzticka", LocalDate.of(2000,11, 2))
        );
        var clients = List.of(
                new Client("Franta Furtdelam", LocalDate.of(2002, 3, 1), LocalDate.of(2023,4, 28)),
                new Client("Norbert Nefachcenko", LocalDate.of(1976, 8, 9), LocalDate.of(2000,1, 1)),
                new Client("Olda Obcasny", LocalDate.of(1987, 10, 2), LocalDate.of(2020,1, 1)),
                new Client("Branislav Brzky", LocalDate.of(1994, 7, 21), LocalDate.of(2012,7, 21)),
                new Client("Franta Furtdelam",  LocalDate.of(1994, 7, 21), LocalDate.of(2023,4, 28))
        );

        clerks.forEach(office::employ);
        if (office.employ(clerks.get(0))) {
            System.out.println("ERROR: clerks already works here!");
        }
        System.out.println();

        System.out.println("Clerks:");
        System.out.println(office.getClerks());
        System.out.println();

        office.assign(clients.get(0), clerks.get(0));
        office.assign(clients.get(1), clerks.get(1));
        office.assign(clients.get(1), clerks.get(1));
        office.assign(clients.get(2), clerks.get(2));
        office.assign(clients.get(3), clerks.get(2));
        office.assign(clients.get(4), clerks.get(2));

        try  {
            office.assign(clients.get(1), new Clerk("Not Employed", LocalDate.of(2000,11, 2))); // non-existing doctor
            System.out.println("ERROR: no such clerk works here");
            System.out.println();
        } catch (IllegalStateException e) {
            // all good
        }

        office.remove(clients.get(4));

        System.out.println("Clients:");
        System.out.println(office.getClients());
        System.out.println();

        System.out.println("Clerks of client:");
        clients.forEach(c -> {
            System.out.println(c.getName() + ": " + office.getClerks(c));
        });
        System.out.println();

        System.out.println("Clients by registration date:");
        System.out.println(office.getClientByDate());
    }
}
