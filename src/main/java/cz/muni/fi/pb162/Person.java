package cz.muni.fi.pb162;

import java.time.LocalDate;

public class Person {
    private final String name;
    private final LocalDate dateOfBirth;


    public Person(String name, LocalDate dateOfBirth) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public String toString() {
       return name + "(" + dateOfBirth + ")";
    }

}
